import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RecommenderService } from './recommender.service';

describe('RecommenderService', () => {
  let service: RecommenderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(RecommenderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
