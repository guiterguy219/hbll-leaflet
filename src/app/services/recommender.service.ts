import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Item, Rating } from '../core/interfaces';

const API_BASE = 'https://api.lib.byu.edu/leaflet';
const RECOMMEND_ITEM_URL = API_BASE + '/item';
const getSaveRatingUrl = (userId: string) => API_BASE + `/users/${userId}/ratings`;

@Injectable({
  providedIn: 'root'
})
export class RecommenderService {
  private userId = 'jcougar123';

  constructor(
    private http: HttpClient,
  ) { }

  public getRecommendation = () => {
    return this.http.get<Item>(RECOMMEND_ITEM_URL);
  }

  public saveRecommendation = (rating: Rating) => {
    return this.http.post<any>(getSaveRatingUrl(this.userId), rating);
  }
}
