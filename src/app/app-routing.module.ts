import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'leaves',
    loadChildren: () =>
      import('./leaves/leaves.module').then(
        (m) => m.LeavesModule
      ),
  },
  {
    path: '**',
    redirectTo: '/leaves',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
