import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item, Rating } from 'src/app/core/interfaces';

@Component({
  selector: 'app-leaf',
  templateUrl: './leaf.component.html',
  styleUrls: ['./leaf.component.scss']
})
export class LeafComponent implements OnInit {
  @Input() public item?: Item;
  @Output() rate = new EventEmitter<Rating>();

  constructor() { }

  ngOnInit(): void {
  }

  public rateItem = (rating: boolean) => {
    this.rate.emit({
      itemId: this.item?.id,
      rating
    });
  }
}
