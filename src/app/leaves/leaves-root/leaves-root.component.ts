import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Item, Rating } from 'src/app/core/interfaces';
import { RecommenderService } from 'src/app/services/recommender.service';

@Component({
  selector: 'app-leaves-root',
  templateUrl: './leaves-root.component.html',
  styleUrls: ['./leaves-root.component.scss']
})
export class LeavesRootComponent implements OnInit, OnDestroy {
  private subs: Subscription[] = [];

  public items: Item[] = [];

  constructor(
    private recommender: RecommenderService,
  ) { }

  ngOnInit(): void {
    for (let i = 0; i < 3; i++) {
      const sub = this.recommender.getRecommendation().subscribe(item => {
        this.items.push(item);
      });
      this.subs.push(sub);
    }
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  public rate = (rating: Rating) => {
    const sub = this.recommender.saveRecommendation(rating).pipe(
      tap(() => {
        this.items.shift();
      }),
      switchMap(() => {
        return this.recommender.getRecommendation();
      })
    ).subscribe(item => this.items.push(item));
    this.subs.push(sub);
  }
}
