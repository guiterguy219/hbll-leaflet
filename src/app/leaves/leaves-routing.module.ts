import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeavesRootComponent } from './leaves-root/leaves-root.component';

const routes: Routes = [
  {
    path: '',
    component: LeavesRootComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeavesRoutingModule { }
