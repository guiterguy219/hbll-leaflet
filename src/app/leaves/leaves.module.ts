import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeavesRoutingModule } from './leaves-routing.module';
import { LeavesRootComponent } from './leaves-root/leaves-root.component';
import { LeafComponent } from './leaf/leaf.component';


@NgModule({
  declarations: [LeavesRootComponent, LeafComponent],
  imports: [
    CommonModule,
    LeavesRoutingModule
  ]
})
export class LeavesModule { }
