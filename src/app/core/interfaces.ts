export interface Item {
    author?: string;
    description: string;
    id: string;
    thumbnail: string;
    title: string;
    type: ItemType;
}

export enum ItemType {
    Book = 'BOOK',
    Film = 'FILM'
}

export interface Rating {
    itemId?: string;
    rating: boolean;
}
